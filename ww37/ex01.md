Exercise: Find online resources (1h)

#### 1. OSI model:
    * https://learningnetwork.cisco.com/s/article/osi-model-reference-chart
    * https://en.wikipedia.org/wiki/OSI_model

#### 2. Switch:
    * https://www.ciscopress.com/articles/article.asp?p=2181836&seqNum=5
    * https://en.wikipedia.org/wiki/Network_switch

#### 3. Router:
    * https://www.ciscopress.com/articles/article.asp?p=2180210&seqNum=4
    * https://en.wikipedia.org/wiki/Router_(computing)

#### 4. Subnet:
    * https://www.cisco.com/c/en/us/support/docs/ip/dynamic-address-allocation-resolution/13711-40.html
    * https://en.wikipedia.org/wiki/Subnetwork

## Sources:
* Cisco is pretty big in networking, they make networking devices and have courses on working with those devices. Their courses are supposed to train people in using their equipment and they make equipment from home devices to corporate network devices.
* Wikipedia is transparent about its sources. The articles get quick and frequent updates.