# Virtualization

Virtualization is simulating a computer on another computer. Virtualization programs are able to separate parts of the computer's hardware to be used by a simulated operating system. Simulating another operating system like that allows quick switching between the main and the simulated systems, allows the testing of programs with arbitrary limitations, allows quarantined running of malicious software and running of programs incompatible with the main operating system. Furthermore it allows the *parallel running of programs*, so it is possible to make changes to one instance while the other is still running and compare those changes in action. *From the perspective of the simulated system it is on its own dedicated hardware*.

Other uses may allow the emulation of specific hardware, even one *based on a different architecture*, *such as a RISC computer*, or a gaming console with the caviat that the host machine needs to be much more powerful than the simulated machine in order to make the simulation real time, or at least close to it.

On a powerful computer virtualization also allows for more efficient use of the available resources by letting several users take advantage of them at the same time, for several different purposes, *such as running different simulations*, or just hosting different web servers.

parallel running of programs:
maatee
I'm trying to find out if it's possible to run programs in parallel on virtual machines, if they are assigned separate CPUs
moozer
"programs"? do you mean virtual machines?
maatee
yes
moozer
try it :-)
(the answer is yes)

From the perspective of the simulated system it is on its own dedicated hardware:
https://www3.technologyevaluation.com/sd/category/virtualization-virtual-machine/articles/virtualization-vs-emulation-vs-simulation

based on a different architecture:
https://serverfault.com/questions/591318/emulating-a-different-cpu-architecture-per-a-vm-on-vmware-esxi

such as a RISC computer:
https://book.rvemu.app/

